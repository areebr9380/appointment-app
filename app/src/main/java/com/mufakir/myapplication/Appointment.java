package com.mufakir.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class Appointment extends AppCompatActivity  {
    EditText organizationName;
    EditText date;
    EditText name;
    EditText fatherName;
    EditText motherName;
    EditText cnic;
    EditText contact;
    EditText address;
    RadioButton male;
    RadioButton female;
    EditText physician;
    EditText specialNote;
    EditText biometric;
    String gender="male";
    SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment);
        male=(RadioButton) findViewById(R.id.radio_one);
        male.setChecked(true);
    }
    public void submitData(View view){
        organizationName=(EditText) findViewById(R.id.E1);
        date=(EditText) findViewById(R.id.E2);
        name=(EditText) findViewById(R.id.E3);
        fatherName=(EditText) findViewById(R.id.E4);
        motherName=(EditText) findViewById(R.id.E5);
        cnic=(EditText) findViewById(R.id.E6);
        contact=(EditText) findViewById(R.id.E7);
        address=(EditText) findViewById(R.id.E8);
        male=(RadioButton) findViewById(R.id.radio_one);
        female=(RadioButton) findViewById(R.id.radio_two);
        physician=(EditText) findViewById(R.id.E10);
        specialNote=(EditText) findViewById(R.id.editText10);
        biometric=(EditText)findViewById(R.id.editText11);
        String result ="";
        String date1=date.getText().toString();
        String dateVal[]=date1.split("-");
        if(dateVal.length==3){
            int arr[]=new int[3];int i;
            for (i=0;i<dateVal.length;i++){
                arr[i]=Integer.parseInt(dateVal[i]);
            }
            if(arr[0]<1000 || arr[1] >12 || arr[2]>31){
                result=result+"Wrong Pattern date inserted ";
            }
        }
        else{
            result=result+"Wrong date Pattern inserted ";
        }
        if(organizationName.getText().toString().equals("")){
            result=result+"No Organization Name ";
        }
        if(date.getText().toString().equals("")){
            result=result+"No Date Entered ";
        }
        if(name.getText().toString().equals("")){
            result=result+"No Name ";
        }
        if(fatherName.getText().toString().equals("")){
            result=result+"No Father Name ";
        }
        if(motherName.getText().toString().equals("")){
            result=result+"No Mother Name ";
        }
        if(cnic.getText().toString().equals("")){
            result=result+"No CNIC Entered ";
        }
        if(contact.getText().toString().equals("")){
            result=result+"No Contact Number ";
        }
        if(address.getText().toString().equals("")){
            result=result+"No Address ";
        }
        if(physician.getText().toString().equals("")){
            result=result+"No Doctor ";
        }
        if(specialNote.getText().toString().equals("")){
            result=result+"No Special Note ";
        }
        if(biometric.getText().toString().equals("")){
            result=result+"No Biometric ";
        }
        if(!result.equals(""))
        {
            Toast.makeText(getApplicationContext(),result,Toast.LENGTH_SHORT).show();
        }
        else{
            SharedPreferences sp = getSharedPreferences("hospital", Context.MODE_PRIVATE);
            SharedPreferences.Editor se=sp.edit();

            int i=sp.getInt("count",0);
            i+=1;
            se=sp.edit();
            se.putString("per","1");
            se.putInt("count",i);
            String h=String.valueOf(i);
            se.putInt("id"+h,i);
            se.putInt("onlineStatus"+h,1);
            se.putString("organizationName"+h,organizationName.getText().toString());
            se.putString("date"+h,date.getText().toString());
            se.putString("name"+h,name.getText().toString());
            se.putString("fatherName"+h,fatherName.getText().toString());
            se.putString("motherName"+h,motherName.getText().toString());
            se.putString("cnic"+h,cnic.getText().toString());
            se.putString("contact"+h,contact.getText().toString());
            se.putString("address"+h,address.getText().toString());
            se.putString("physician"+h,physician.getText().toString());
            se.putString("specialNote"+h,specialNote.getText().toString());
            se.putString("biometric"+h,biometric.getText().toString());
            se.putString("gender"+h,gender);
            se.apply();int b;
            startActivity(new Intent(getApplicationContext(),showData.class));

/*
            // The connection URL
            String url = "http://202.";

            // Create a new RestTemplate instance
            RestTemplate restTemplate = new RestTemplate();

            // Add the String message converter
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

            // Make the HTTP GET request, marshaling the response to a String
            String result = restTemplate.getForObject(url, String.class, "Android");
            */

        }



    }
    public void checkfemale(View view){
        ((RadioGroup) findViewById(R.id.radioGroup)).clearCheck();
        male=(RadioButton) findViewById(R.id.radio_one);
        female=(RadioButton) findViewById(R.id.radio_two);
        gender="female";
           male.setChecked(false);

//            female.setChecked(true);
//            Toast.makeText(getApplicationContext(),"Female working",Toast.LENGTH_SHORT).show();
    }
    public void checkmale(View view){

        male=(RadioButton) findViewById(R.id.radio_one);
        female=(RadioButton) findViewById(R.id.radio_two);
        female.setChecked(false);
        gender="male";


    }
    public void showData(View view){
        startActivity(new Intent(getApplicationContext(),showData.class));
    }
}
