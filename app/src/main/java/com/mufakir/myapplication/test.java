package com.mufakir.myapplication;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.stream.Collectors;


public class test extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void btnclick(View view){
        TextView tv = (TextView) findViewById(R.id.tv1);
        //tv.setText("Hello");

        String a = "Error";

        //http://202.141.248.54/SMSReaderService.asmx/HelloWorldhttp://qalaqand.com/android/test.php
//response - Hello World
        new Synctask().execute("http://qalaqand.com/android/test.php");




    }
    class Synctask extends AsyncTask<String,Void,String> {


        @Override
        protected String doInBackground(String... params) {
            String s=null;
            URL url= null;
            try {
                url = new URL(params[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


            try {
                HttpURLConnection httpURLConnection =(HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                Uri.Builder ub=new Uri.Builder().appendQueryParameter("Name","MyName").appendQueryParameter("Address","myaddress").appendQueryParameter("MobNo","1425252525").appendQueryParameter("CNIC","5555555555555").appendQueryParameter("Gender","Male").appendQueryParameter("Org","my org").appendQueryParameter("Father","my father").appendQueryParameter("Mother","my mother").appendQueryParameter("Phys","my Phys").appendQueryParameter("Note","my note").appendQueryParameter("Date","2019-10-09");


                long length = 0;
                String line;
                OutputStream outputStream=httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                bufferedWriter.write(ub.build().getEncodedQuery());
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                httpURLConnection.connect();
                InputStream inputStream=httpURLConnection.getInputStream();
                BufferedReader bufferedReader= new BufferedReader(new InputStreamReader(inputStream));
                //s =bufferedReader.readLine();


                while ((line = bufferedReader.readLine()) != null) {
                    length += line.length();
                    s = s + line;
                }
                //System.out.println("Read length: " + length);


                bufferedReader.close();
                return s;


            } catch (IOException e) {
                e.printStackTrace();
            }


            return s;}

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Toast.makeText(getApplicationContext(),s,Toast.LENGTH_SHORT).show();

        }
    }

}
