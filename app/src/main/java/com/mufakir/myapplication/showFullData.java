package com.mufakir.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class showFullData extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_full_data);
        Intent intent=getIntent();
        String id=intent.getStringExtra("id");
        TextView textView1=(TextView)findViewById(R.id.textView10);
        TextView textView2=(TextView)findViewById(R.id.textView12);
        TextView textView3=(TextView)findViewById(R.id.textView14);
        TextView textView4=(TextView)findViewById(R.id.textView16);
        TextView textView5=(TextView)findViewById(R.id.textView1610);
        TextView textView6=(TextView)findViewById(R.id.textView1810);
        TextView textView7=(TextView)findViewById(R.id.textView2010);
        TextView textView8=(TextView)findViewById(R.id.textView2210);
        TextView textView9=(TextView)findViewById(R.id.textView2410);
        TextView textView10=(TextView)findViewById(R.id.textView2610);
        TextView textView11=(TextView)findViewById(R.id.textView2810);
        TextView textView12=(TextView)findViewById(R.id.textView3010);
        SharedPreferences sp = getSharedPreferences("hospital", Context.MODE_PRIVATE);

        textView1.setText(sp.getString("organizationName"+id,""));
        textView2.setText(sp.getString("date"+id,""));
        textView3.setText(sp.getString("name"+id,""));
        textView4.setText(sp.getString("fatherName"+id,""));
        textView5.setText(sp.getString("motherName"+id,""));
        textView6.setText(sp.getString("cnic"+id,""));
        textView7.setText(sp.getString("contact"+id,""));
        textView8.setText(sp.getString("address"+id,""));
        textView9.setText(sp.getString("physician"+id,""));
        textView10.setText(sp.getString("specialNote"+id,""));
        textView11.setText(sp.getString("biometric"+id,""));
        textView12.setText(sp.getString("gender"+id,""));
    }
}
