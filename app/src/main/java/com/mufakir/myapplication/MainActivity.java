package com.mufakir.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



    }
    public void validate(View view){
        String[] userName={"admin@abc.com","faculty@abc.com","user@abc.com"};
        String[] password={"","fac123","user123"};
        EditText editText=(EditText) findViewById(R.id.et);
        EditText editText1=(EditText) findViewById(R.id.et1);
        int b;
        boolean bool =false;
        for (b=0;b<userName.length;b++){
            if(editText.getText().toString().equals(userName[b]) && editText1.getText().toString().equals(password[b])  ){
                bool=true;
            }
        }
        if(bool){
            startActivity(new Intent(getApplicationContext(),Appointment.class));
            finish();
        }
        else{
            Toast.makeText(getApplicationContext(),"Invalid User!",Toast.LENGTH_SHORT).show();
        }
    }
}
