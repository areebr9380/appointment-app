package com.mufakir.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class showData extends AppCompatActivity implements AdapterView.OnItemClickListener {

    int ids[];

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_data);
        ListView listView=(ListView)findViewById(R.id.lv);
        ArrayList<String> arrayList=new ArrayList<>();
        SharedPreferences sp = getSharedPreferences("hospital", Context.MODE_PRIVATE);
        int i=sp.getInt("count",0);
        int n=i;
        i=i-sp.getInt("success",0);
        int b;
        String[] arr=new String[i];
        ids=new int[i];int c=0;
        for (b=1;b<=n;b++){

            if(sp.getInt("onlineStatus"+String.valueOf(b),1)==1) {
                ids[c]=b;
                arr[c] = sp.getString("organizationName" + String.valueOf(b), "");
                c+=1;
                arrayList.add(sp.getString("organizationName" + String.valueOf(b), ""));
            }
        }
        ArrayAdapter arrayAdapter=new ArrayAdapter(this,android.R.layout.simple_expandable_list_item_1,arr);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(getApplicationContext(),showFullData.class);
                int buff=ids[position];
                intent.putExtra("id",String.valueOf(buff));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    }
    public void syncAll(View view){
        SharedPreferences sp = getSharedPreferences("hospital", Context.MODE_PRIVATE);
        int b,result=sp.getInt("count",0);
        for (b=1;b<=result;b++){

            if(sp.getInt("onlineStatus"+String.valueOf(b),1)==1){
                String orgName=sp.getString("organizationName"+String.valueOf(b),"");
                String date=sp.getString("date"+String.valueOf(b),"");
                String name=sp.getString("name"+String.valueOf(b),"");
                String fname=sp.getString("fatherName"+String.valueOf(b),"");
                String mname=sp.getString("motherName"+String.valueOf(b),"");
                String cnic=sp.getString("cnic"+String.valueOf(b),"");
                String contact=sp.getString("contact"+String.valueOf(b),"");
                String address=sp.getString("address"+String.valueOf(b),"");
                String phy=sp.getString("physician"+String.valueOf(b),"");
                String specialNote=sp.getString("specialNote"+String.valueOf(b),"");
                String biometric=sp.getString("biometric"+String.valueOf(b),"");
                String gender=sp.getString("gender"+String.valueOf(b),"");
                String apiId= String.valueOf(sp.getInt("id"+String.valueOf(b),1));


                new Synctask().execute("http://203.135.51.46/SMSReaderService.asmx/SavePatRec",orgName,date,name,fname,mname,cnic,contact,address,phy,specialNote,biometric,gender,apiId);


            }

        }

    }
    class Synctask extends AsyncTask<String,Void,String> {


        @Override
        protected String doInBackground(String... params) {
            String s="";
            URL url= null;
            try {
                url = new URL(params[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


            try {
                HttpURLConnection httpURLConnection =(HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                Uri.Builder ub=new Uri.Builder().appendQueryParameter("Name",params[3]).appendQueryParameter("Address",params[8]).appendQueryParameter("MobNo",params[7]).appendQueryParameter("CNIC",params[7]).appendQueryParameter("Gender",params[12]).appendQueryParameter("Org",params[1]).appendQueryParameter("Father",params[4]).appendQueryParameter("Mother",params[5]).appendQueryParameter("Phys",params[9]).appendQueryParameter("Note",params[10]).appendQueryParameter("Date",params[2]);


                long length = 0;
                String line="";
                OutputStream outputStream=httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                bufferedWriter.write(ub.build().getEncodedQuery());
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                httpURLConnection.connect();
                InputStream inputStream=httpURLConnection.getInputStream();
                BufferedReader bufferedReader= new BufferedReader(new InputStreamReader(inputStream));
                s =bufferedReader.readLine();

                while ((line = bufferedReader.readLine()) != null) {
                    length += line.length();
                    s = s + line;


                }


                //System.out.println("Read length: " + length);


                bufferedReader.close();

                return s+"~"+params[13];


            } catch (IOException e) {
                e.printStackTrace();

                               }

            return s;}

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


                String[] array = s.split("~");
                if (array.length >= 2) {
                    if (array[0].trim().equals(
                            "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                            "<string xmlns=\"http://tempuri.org/\">Success</string>")) {
                        SharedPreferences sp = getSharedPreferences("hospital", Context.MODE_PRIVATE);
                        SharedPreferences.Editor se = sp.edit();
                        se.putInt("onlineStatus" + array[1], 0);
                        int x = sp.getInt("success", 0);
                        x += 1;
                        se.putInt("success", x);
                        se.apply();
                        Toast.makeText(getApplicationContext(),"Data Submitted Successfully!",Toast.LENGTH_SHORT).show();

                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Something Went Wrong",Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(getApplicationContext(),"Something Went Wrong",Toast.LENGTH_SHORT).show();
                }



        }




    }


}
